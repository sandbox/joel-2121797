<?php

/*
 * @file
 * Helper functions for the pre module
 */


/*
 * Get path to current page
 */
function pre_current_path() {
  return request_uri();
}

/*
 * Logging helper
 */
function pre_log($status, $message) {
  drupal_set_message(check_plain($message), $status);
}
