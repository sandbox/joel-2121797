<?php

/**
 * @file
 *
 * This is the main module file.
 */

define('PRE_DEFAULT_GA_MAX_SOURCES', 1000);
define('PRE_GA_MAX_DESTINATIONS', 5);
define('PRE_GA_REPORT_ITEMS_PER_PAGE', 50);



/**
 * Implementation of hook_menu()
 */
function pre_ga_menu() {
  $items = array();

  $items['admin/config/system/pre_ga'] = array(
    'title' => 'Pre: Google Analytics',
    'description' => 'Configure pre_ga module settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pre_ga_admin'),
    'access arguments' => array('administer pre'),
  );

  $items['admin/reports/pre_ga'] = array(
    'title' => 'Pre: Google Analytics Summary',
    'description' => 'View a summary of your Google Analytics page data.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pre_ga_admin_summary'),
    'access arguments' => array('administer pre'),
  );

  return $items;
}

/**
 * Implements hook_cron()
 */
function pre_ga_cron() {
  // Default to a daily interval
  $interval = variable_get('pre_ga_interval', 86400);

  // We usually don't want to act every time cron runs (which could be every
  // minute) so keep a time for the next run in a variable.
  if (time() >= $interval + variable_get('pre_ga_last_execution', 0)) {
    $result = pre_update_ga();
    if ($result) {
      watchdog('pre_ga', 'pre_ga_cron executed');
      variable_set('pre_ga_last_execution', time());
    }
  }
}

/**
 * Get all path data for reporting purposes
 */
function pre_ga_get_path_data() {
    $result = db_select('pre_ga_visitor_flow', 'p')
    ->fields('p', array('source_path', 'destination_path', 'probability'))
    ->extend('PagerDefault')
    ->orderBy('probability', 'DESC')
    ->limit(PRE_GA_REPORT_ITEMS_PER_PAGE)
    ->execute();
  if ($result->rowCount() > 0) {
    $rows = array();
    while ($row = $result->fetchAssoc()) {
      $rows[] = $row;
    }
    return $rows;
  }
  else {
    return FALSE;
  }
}

/*
 * Get next page data for a given path
 */
function pre_ga_next_probability($path, $limit = PRE_MAX_DESTINATIONS) {
  $result = db_select('pre_visitor_flow', 'p')
    ->fields('p', array('source_path', 'destination_path', 'probability'))
    ->condition('source_path', $path, 'LIKE')
    ->orderBy('probability', 'DESC')
    ->range(0, $limit)
    ->execute();
  if ($result->rowCount() > 0) {
    $rows = array();
    while ($row = $result->fetchAssoc()) {
      $rows[] = array(
        'destination_path' => $row['destination_path'],
        'probability' => $row['probability'],
      );
    }
    return $rows;
  }
  else {
    return FALSE;
  }
}

/*
 * Fetch and save GA visitor flow data to local database
 */
function pre_ga_update() {
  $profile = variable_get('google_analytics_reports_profile_id', 0);
  $data = pre_ga_get_visitor_flow();

  if (is_array($data)) {
    foreach ($data as $row) {
      // insert/update probability value for key [source, destination]
      db_merge('pre_visitor_flow')
        ->key(array(
          'source_path' => $row['source'],
          'destination_path' => $row['destination'],
        ))
        ->fields(array(
          'probability' => $row['probability'],
        ))
        ->execute();
    }

    pre_log('status', t('Successfully processed GA data for profile @profile.', array('@profile' => $profile)));
  }
  else {
    pre_log('error', t('Error. Could not process GA data for profile @profile.', array('@profile' => $profile)));
    return FALSE;
  }
  return TRUE;
}

/**
 * Retrieves visitor flow data
 */
function pre_ga_get_visitor_flow() {
  $data = array();
  $params = array(
    'metrics' => array('ga:pageviews'),
    'dimensions' => array('ga:pagePath'),
    'sort_metric' => array('-ga:pageviews'),
    'start_date' => strtotime('-31 days'),
    'end_date' => strtotime('-1 day'),
    'sort' => '-ga:pageviews',
    'max_results' => variable_get('pre_ga_max_sources', PRE_DEFAULT_GA_MAX_SOURCES),
  );
  $source_feed = google_analytics_api_report_data($params);
  if ($source_feed->error || !is_array($source_feed->results)) {
    return FALSE;
  }
  foreach ($source_feed->results as $source_row) {
    $params = array(
      'metrics' => array('ga:pageviews'),
      'dimensions' => array('ga:pagePath'),
      'sort_metric' => array('-ga:pageviews'),
      'start_date' => strtotime('-31 days'),
      'end_date' => strtotime('-1 day'),
      'sort' => '-ga:pageviews',
      'max_results' => PRE_GA_MAX_DESTINATIONS,
      'filters' => _pre_ga_path_filter($source_row['pagePath'], 'ga:previousPagePath'),
    );
    $dest_feed = google_analytics_api_report_data($params);
    if ($dest_feed->error) {
      return FALSE;
    }
    foreach ($dest_feed->results as $dest_row) {
      // skip if paths match
      if ($source_row['pagePath'] == $dest_row['pagePath']) {
        continue;
      }
      $probability = $dest_row['pageviews'] / $source_row['pageviews'];
      $data[] = array(
        'source' => $source_row['pagePath'],
        'destination' => $dest_row['pagePath'],
        'probability' => $probability,
      );
    }
  }
  return $data;
}

/*
 * Construct a filter string that grabs pagePaths corresponding to the specified path.
 */
function _pre_ga_path_filter($path, $type = 'ga:pagePath') {
  // Google Analytics regex filters have a limit of 32 characters. Therefore we
  // can't simply create one filter per pagePath. Instead we are going too do a
  // "contains substring" match on the path, and then take as many of the ending
  // characters paired with ([?#].*)?$.

  // Use 100 character maximum to allow some room for escaping regex metacharacters.
  if (strlen($path) <= 121) {
    $filter = "$type=~^" . preg_quote($path) . '(#.*)?$';
  }
  else {
    $filter = "$type=@$path;$type=~" . preg_quote(substr($path, -100)) . '(#.*)?$';
  }
  return $filter;
}

/*
 * Implements hook_requirements().
 */
function pre_ga_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break during installation.
  $t = get_t();

  $requirements['pre_ga'] = array();

  // Check pre cron status during runtime
  if ($phase == 'runtime') {
    $last = variable_get('pre_ga_last_execution');
    if (is_numeric($last)) {
      $requirements['pre_ga']['severity'] = REQUIREMENT_OK;
      $requirements['pre_ga']['value'] = $t('Last run !time ago', array('!time' => format_interval(REQUEST_TIME - $last)));
    }
    else {
      $requirements['pre_ga'] = array(
        'severity' => REQUIREMENT_WARNING,
        'value' => $t('Never run'),
      );
    }

    $requirements['pre_ga']['title'] = $t('pre_ga cron tasks');
  }

  return $requirements;
}


/**
 * Menu callback - admin form
 */
function pre_ga_admin() {
  $form = array();

  $options = array();
  foreach (array(10, 100, 250, 500, 1000, 2500, 5000, 10000) as $i) {
    $options[$i] = number_format($i);
    if ($i == PRE_DEFAULT_GA_MAX_SOURCES) {
      $options[$i] .= ' (default)';
    }
  }
  $form['pre_ga_max_sources'] = array(
    '#type' => 'select',
    '#title' => 'GA Max Sources',
    '#description' => 'The maximum number of pages to fetch GA report data (i.e. fetch most visited <em>n</em> pages). Since a sub-query is used, higher values can be long running.',
    '#options' => $options,
    '#default_value' => variable_get('pre_ga_max_sources', PRE_DEFAULT_GA_MAX_SOURCES),
  );

  return system_settings_form($form);
}

/**
 * Menu callback - admin summary report
 */
function pre_ga_admin_summary() {
  $page = array();
  $data = pre_ga_get_path_data();

  if (empty($data)) {
    drupal_set_message(
      t('You don\'t have any Google Analytics report data available yet. Have you configured <a href="@ga_url">Google Analytics Reports</a>? Check the <a href="@status_url">status report</a> to see if pre cron task has run.',
        array(
          '@ga_url' => url('admin/config/system/google-analytics-reports'),
          '@status_url' => url('admin/reports/status'),
        )
      ),
      'warning'
    );
    return $page;
  }

  $header = array(
    'Source',
    'Destination',
    'Probability',
  );

  // format probability numbers
  foreach ($data as &$row) {
    $row['probability'] = number_format(round($row['probability'] * 100, 2), 2) . '%';
  }

  $page['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $data,
  );

  $page['pager'] = array(
    '#theme' => 'pager',
  );

  return $page;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 *
 * Alter the existing Pre settings on the node form.
 */
function pre_ga_form_node_form_alter(&$form, &$form_state) {
  $node = $form_state['node'];

  // Add note to the Prerender URL field letting users know that they can
  // override Pre GA settings by specifying a URL.
  $form['pre']['pre_prerender_url']['#description'] .= t('<em>Leave blank to use default behavior based on visitor data.</em>');
}

/**
 * Implements THEME_preprocess_html().
 *
 * This function queries our pre-generated GA prerender data and inserts the
 * <link> tag if necessary.
 */
function pre_ga_preprocess_html(&$variables) {
  $prerender_path = null;
  $next_data = pre_next_probability(pre_current_path());

  // Check paths against probability threshold.
  if (is_array($next_data)) {
    foreach ($next_data as $next) {
      if (($next['probability'] * 100) > variable_get('pre_prerender_probability', PRE_DEFAULT_PRERENDER_PROBABILITY)) {
        $prerender_path = $next['destination_path'];
        break;
      }
    }
  }
}
